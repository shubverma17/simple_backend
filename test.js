var http = require('http');
var url = require('url');
var querystring = require('querystring');


function onRequest(req, res) {

	if (req.url == "/listTestData")
	{
		res.writeHead(200, { "Content-Type": "application/json",
							"Access-Control-Allow-Origin": "*" });
		res.header
		
		var chartvalues =JSON.stringify({"chartvalues":[
				          {
				            "label" : "A" ,
				            "value" : -29.765957771107
				          } ,
				          {
				            "label" : "B" ,
				            "value" : 0
				          } ,
				          {
				            "label" : "C" ,
				            "value" : 32.807804682612
				          } ,
				          {
				            "label" : "D" ,
				            "value" : 196.45946739256
				          } ,
				          {
				            "label" : "E" ,
				            "value" : 0.19434030906893
				          } ,
				          {
				            "label" : "F" ,
				            "value" : -98.079782601442
				          } ,
				          {
				            "label" : "G" ,
				            "value" : -13.925743130903
				          } ,
				          {
				            "label" : "H" ,
				            "value" : -5.1387322875705
				          }
				        ]});

		res.end(chartvalues);
		
	}
	
	// 404'd!
	else {
		res.writeHead(404, { "Content-Type": "text/plain" });
		res.end("404 error! File not found.");
	}
}

http.createServer(onRequest).listen(7000);